# Sanic

## Клонирование проекта

HTTPS:

```git clone https://gitlab.com/sandlabs/templates/sanic.git```

Вписать логин и пароль

SSH:

Прокинуть SSH ключи в свой аккаунт и прописать

```git clone git@gitlab.com:sandlabs/templates/sanic.git```


## Как запускать

1. Прописать команды export:

```export SANIC_PG_CONNECTION=<Postgres connection>```

```export SANIC_REDIS_CONNECTION=<Redis connection>```

2. Установить библиотеки с помощью poetry:

```sudo -H python3 -m pip install -U poetry```

```python3 -m venv .venv```



```source .venv/bin/activate```

```poetry install```

3. Запустить API

```sanic main.app --host=0.0.0.0 --port=7474 --workers=1 --access-logs -r```

## Как открыть Swagger

В адресной строке вбить ```0.0.0.0:7474/docs/swagger```

## Как запустить тесты

```export SANIC_PG_CONNECTION=<TEST POSTGERSQL>```

```export SANIC_PG_VERSION=<POSTGRESQL_VERSION>```

Зайти в папку tests и прописать ```pytest```

## Как заливать на Git

1. ```git add .```

2. ```git commit -m '<Комментарий>'```

3. ```git push```