from enum import Enum
from typing import Dict

from sqlalchemy.dialects.postgresql import ENUM

from .base_model import BaseModel
from .db import db


class UserRole(Enum):
    ADMIN = 'ADMIN'
    USER = 'USER'


class UserLang(Enum):
    EN = 'EN'
    RU = 'RU'


class User(BaseModel):
    __tablename__ = 'users'
    __hiden_keys__ = (
        'is_deleted',
        'password',
    )

    role = db.Column(ENUM(UserRole, name='user_roles'), nullable=False, index=True, default=UserRole.USER, server_default=UserRole.USER.value, comment='User role')  # noqa
    lang = db.Column(ENUM(UserLang, name='user_langs'), nullable=False, index=True, default=UserLang.EN, server_default=UserLang.EN.value, comment='User lang')  # noqa

    email = db.Column(db.String(), nullable=False, unique=True, comment='User email')  # noqa
    password = db.Column(db.String(), nullable=False, comment='User password')  # noqa

    @property
    def is_admin(self):
        return self.role == UserRole.ADMIN

    @property
    def is_user(self):
        return self.role == UserRole.USER

    def to_dict(self, del_hiden_keys: bool = True) -> Dict:  # pylint: disable=arguments-differ
        user_dict = super().to_dict(del_hiden_keys)
        return user_dict
