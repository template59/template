from datetime import datetime
from uuid import uuid4

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func

from .db import db


class BaseModel(db.Model):
    __abstract__ = True

    id = db.Column(UUID(), primary_key=True, default=uuid4, server_default=func.uuid_generate_v4(), comment='ID')  # noqa
    create_datetime = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow, server_default=func.now(), comment='UTC create datetime')  # noqa
    update_datetime = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow, server_default=func.now(), comment='UTC update datetime')  # noqa
    is_deleted = db.Column(db.Boolean(), nullable=False, default=False, server_default='f', index=True, comment='Is deleted')  # noqa

    async def mark_deleted(self):
        await self.update(is_deleted=True, update_datetime=datetime.utcnow()).apply()
