from datetime import datetime
from typing import Optional

from sqlalchemy.sql import Select

from ..models.db import CRUDModel


def is_deleted_query(query: Select, model: CRUDModel, is_deleted: bool = False) -> Select:
    return query.where(model.is_deleted == is_deleted)


def datetime_query(
    query: Select,
    model: CRUDModel,
    from_datetime: Optional[datetime] = None,
    to_datetime: Optional[datetime] = None
) -> Select:
    if from_datetime and to_datetime and from_datetime == to_datetime:
        query = query.where(model.create_datetime == from_datetime)
    else:
        if from_datetime:
            query = query.where(model.create_datetime >= from_datetime)
        if to_datetime:
            query = query.where(model.create_datetime <= to_datetime)
    return query


def limit_query(
    query: Select,
    limit: Optional[int] = None,
    offset: Optional[int] = None
) -> Select:
    if limit is not None:
        query = query.limit(limit)
    if offset is not None:
        query = query.offset(offset)
    return query
