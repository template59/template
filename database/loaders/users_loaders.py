from datetime import datetime
from typing import Optional

from gino.api import GinoExecutor

from ..models import User
from .base_loaders import datetime_query, is_deleted_query, limit_query


default_user_columns = (
    User.id,
    User.create_datetime,
    User.update_datetime,
    User.is_deleted,
    User.role,
    User.lang,
    User.email,
    User.password,
)


def users_query(
    user_id: Optional[str] = None,
    email: Optional[str] = None,
    is_deleted: bool = False,
    from_datetime: Optional[datetime] = None,
    to_datetime: Optional[datetime] = None,
    limit: Optional[int] = None,
    offset: Optional[int] = None,
    role: Optional[str] = None,
) -> GinoExecutor:
    query = User.query.order_by(User.create_datetime.desc())

    if user_id:
        query = query.where(User.id == user_id)

    if email:
        query = query.where(User.email == email)

    if role:
        query = query.where(User.role == role)

    query = is_deleted_query(query, User, is_deleted)
    query = datetime_query(query, User, from_datetime, to_datetime)
    query = limit_query(query, limit, offset)
    return query.gino
