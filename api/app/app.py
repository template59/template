from sanic import Blueprint, Sanic

from . import config


def create_app(config_object: object = config.Config, need_register_extensions: bool = True) -> Sanic:
    app = Sanic(config_object.APP_NAME)
    app.config.load(config_object)
    app.config.load_environment_vars()
    if need_register_extensions:
        register_extensions(app)
    register_blueprints(app)
    return app


def register_extensions(app: Sanic):
    from . import extensions  # pylint: disable=import-outside-toplevel
    extensions.register_db(app)
    extensions.register_redis(app)
    extensions.register_password_hasher(app)
    extensions.register_jwt(app)


def register_blueprints(app: Sanic):
    from . import blueprints  # pylint: disable=import-outside-toplevel
    app.blueprint(Blueprint.group(
        blueprints.healthcheck.blueprint,
        blueprints.users.blueprint,
        url_prefix=app.config.APP_URL_PREFIX
    ))
