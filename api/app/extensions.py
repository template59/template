from decimal import Decimal

import aioredis
import sentry_sdk
from alembic.command import upgrade as alembic_upgrade
from alembic.config import Config as AlembicConfig
from sanic import Sanic
from sentry_sdk.integrations.logging import LoggingIntegration
from sentry_sdk.integrations.sanic import SanicIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration

from database import db

from .utils import aioutils, password_hasher
from .utils.jwt import JWT


def register_db(app: Sanic):
    async def migrate_db(app: Sanic, _):
        alembic_config = AlembicConfig(app.config.ALEMBIC_INI_PATH)
        alembic_config.set_main_option('script_location', app.config.ALEMBIC_SCRIPTS_PATH)
        alembic_config.set_main_option('sqlalchemy.url', app.config.PG_CONNECTION)
        await aioutils.run_in_executor(alembic_upgrade, alembic_config, 'head')

    app.register_listener(migrate_db, 'before_server_start')

    app.config.DB_DSN = app.config.PG_CONNECTION
    app.config.DB_ECHO = app.config.DEBUG
    db.init_app(app)
    app.ctx.db = db


def register_redis(app: Sanic):
    async def create_redis_connection(app: Sanic, _):
        app.ctx.redis = await aioredis.from_url(app.config.REDIS_CONNECTION)

    async def close_redis_connection(app: Sanic, _):
        if hasattr(app.ctx, 'redis'):
            await app.ctx.redis.close()

    app.register_listener(create_redis_connection, 'before_server_start')
    app.register_listener(close_redis_connection, 'before_server_stop')


def register_password_hasher(app: Sanic):
    app.ctx.password_hasher = password_hasher.PasswordHasher()


def register_jwt(app: Sanic):
    app.ctx.jwt = JWT(app)
    app.ext.openapi.add_security_scheme('token', 'http', scheme='bearer', bearer_format='JWT')
