import os
from typing import Dict, Tuple

import toml
from sanic.log import LOGGING_CONFIG_DEFAULTS


app_dir = os.path.abspath(os.path.dirname(__file__))
root_dir = os.path.abspath(os.path.join(app_dir, os.pardir, os.pardir))
project_root = os.path.abspath(os.path.join(app_dir, os.pardir))

pyproject_info = toml.load(os.path.join(project_root, 'pyproject.toml'))
poetry_info = pyproject_info['tool']['poetry']

app_name = poetry_info['name']
app_version = poetry_info['version']
app_description = poetry_info['description']


LOGGING_CONFIG_DEFAULTS['loggers'].update({
    'gino': {'level': 'ERROR', 'handlers': ['console']},
    'alembic.runtime.migration': {'level': 'INFO', 'handlers': ['console']}
})


class Config:
    PROJECT_NAME: str = 'Test'
    APP_NAME: str = app_name
    APP_VERSION: str = app_version
    APP_URL_PREFIX: str = '/api'
    APP_DIR: str = app_dir
    PROJECT_ROOT: str = project_root
    AUTO_EXTEND: bool = True
    DEBUG: bool = False
    TESTS: bool = False
    ENVIRONMENT: str = 'review'
    PROXIES_COUNT: int = 0  # If use NGINX set to 1
    FALLBACK_ERROR_FORMAT = 'json'

    PG_CONNECTION: str = None
    REDIS_CONNECTION: str = None

    SANIC_JWT_SECRET: str = f'{app_name} jwt super-super secret'
    SANIC_JWT_STRICT_SLASHES: bool = True
    SANIC_JWT_PATH_TO_AUTHENTICATE: str = ''
    SANIC_JWT_BLUEPRINT_NAME: str = 'auth'
    SANIC_JWT_URL_PREFIX: str = f'{APP_URL_PREFIX}/{SANIC_JWT_BLUEPRINT_NAME}'
    SANIC_JWT_COOKIE_SET: bool = True
    SANIC_JWT_COOKIE_STRICT: bool = False
    SANIC_JWT_EXPIRATION_DELTA: int = 7 * 24 * 60 * 60
    SANIC_JWT_USER_ID: str = 'id'
    SANIC_JWT_REFRESH_TOKEN_ENABLED: bool = False

    ALEMBIC_INI_PATH: str = os.path.join(project_root, 'database', 'alembic.ini')
    ALEMBIC_SCRIPTS_PATH: str = os.path.join(project_root, 'database', 'alembic')

    API_TITLE: str = 'Sanic'
    API_VERSION: str = app_version
    API_DESCRIPTION: str = app_description
    SWAGGER_UI_CONFIGURATION: Dict[str, str] = {'operationsSorter': 'method'}

    CORS_AUTOMATIC_OPTIONS: bool = True
    CORS_SUPPORTS_CREDENTIALS: bool = True
    CORS_ORIGINS: str = '.*'
    CORS_ALLOW_HEADERS: Tuple[str] = (
        'accept',
        'accept-encoding',
        'authorization',
        'content-type',
        'dnt',
        'origin',
        'user-agent',
        'x-csrftoken',
        'x-requested-with'
    )
    CORS_SEND_WILDCARD: bool = False
