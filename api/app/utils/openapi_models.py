from pydantic import BaseModel
from sanic_ext import openapi


class ResponseSchema:
    description = openapi.String(description='Description')
    status = openapi.Integer(description='Status code')
    message = openapi.String(description='Message')


class AuthErrorSchema:
    reasons = openapi.Array(openapi.String(description='Reason'))
    exception = openapi.String(description='Exception')


class AuthModel(BaseModel):
    email: str
    password: str


class AuthSchema:
    email = openapi.String(description='Login')
    password = openapi.String(description='Password')


class AuthTokensSchema:
    access_token = openapi.String(description='Access Token')
    refresh_token = openapi.String(description='Refresh Token')


class UserSchema:
    id = openapi.String(description='User ID', format='uuid')
    email = openapi.String(description='User login')
    role = openapi.String(description='User role', oneOf=('ADMIN', 'USER'), example='USER')
    create_datetime = openapi.DateTime(description='User ISO create datetime')
    update_datetime = openapi.DateTime(description='User ISO update datetime')


class CurrentUserSchema:
    me = UserSchema


class AuthVerifySchema:
    valid = openapi.Boolean(description='Is valid authentication')


class AccessTokenSchema:
    access_token = openapi.String(description='Access token')


class RefreshTokenSchema:
    refresh_token = openapi.String(description='Refresh token')
