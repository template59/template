from sanic import Blueprint
from sanic.request import Request
from sanic.response import json
from sanic_ext import openapi

from app.utils.openapi_models import ResponseSchema


blueprint = Blueprint('healthcheck', url_prefix='/healthcheck', strict_slashes=True)


@blueprint.get('')
@openapi.operation('getHealthcheck')
@openapi.summary('Get healthcheck')
@openapi.response(200, {'application/json': ResponseSchema}, description='OK')
@openapi.response(500, {'application/json': ResponseSchema}, description='Internal Server Error')
async def get_healthcheck(request: Request):  # pylint: disable=unused-argument
    return json({'status': 200, 'message': 'OK', 'description': None})
