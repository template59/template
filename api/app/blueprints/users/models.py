from typing import Optional

from pydantic import BaseModel, validator
from sanic_ext import openapi


class UserSchema:
    id = openapi.String(description='User ID', format='uuid')
    email = openapi.String(description='User email')
    role = openapi.String(description='User role')
    create_datetime = openapi.DateTime(description='User ISO create datetime')
    update_datetime = openapi.DateTime(description='User ISO update datetime')


class GetUsersSchema:
    limit = openapi.Integer(description='Users limit', required=False, nullable=False)
    offset = openapi.Integer(description='Users offset', required=False, nullable=False)


class GetUsersModel(BaseModel):
    limit: Optional[int]
    offset: Optional[int] = 0

    @validator('*', pre=True)
    def first_arg(cls, value):  # noqa
        if isinstance(value, list):
            return value[0]
        return value


class AddUserSchema:
    email = openapi.String(description='User email')
    password = openapi.String(description='User password')
    role = openapi.String(description='User role')


class AddUserModel(BaseModel):
    email: str
    password: str
    role: str


class UpdateUserSchema:
    email = openapi.String(description='New email')
    new_password = openapi.String(description='New Password')
    role = openapi.String(description='User role', oneOf=('ADMIN', 'USER'), required=False, nullable=False)


class UpdateUserModel(BaseModel):
    email: str
    new_password: str
    role = str


class DeleteUser:
    user_id = openapi.String(description='User ID', format='uuid')


class DeleteUserModel(BaseModel):
    user_id: str
