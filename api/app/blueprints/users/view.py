from datetime import datetime

from sanic import Blueprint
from sanic.exceptions import InvalidUsage
from sanic.request import Request
from sanic.response import json
from sanic_ext import openapi, validate
from sanic_jwt.decorators import inject_user, protected, scoped

from app.utils.openapi_models import AuthErrorSchema, ResponseSchema
from database import User, UserRole, loaders

from .models import (
    AddUserModel, AddUserSchema, GetUsersModel, GetUsersSchema, UpdateUserModel, UpdateUserSchema, UserSchema
)


blueprint = Blueprint('users', url_prefix='/users', strict_slashes=True)


@blueprint.get('')
@openapi.operation('getUsers')
@openapi.summary(f'[{UserRole.ADMIN.value}] Get users')
@openapi.secured('token')
@openapi.parameter('offset', GetUsersSchema.offset)
@openapi.parameter('limit', GetUsersSchema.limit)
@openapi.response(200, {'application/json': openapi.Array(UserSchema)}, description='OK')
@openapi.response(400, {'application/json': ResponseSchema}, description='Bad Request')
@openapi.response(401, {'application/json': AuthErrorSchema}, description='Unauthorized')
@openapi.response(403, {'application/json': AuthErrorSchema}, description='Forbidden')
@openapi.response(404, {'application/json': ResponseSchema}, description='Not Found')
@openapi.response(500, {'application/json': ResponseSchema}, description='Internal Server Error')
@validate(query=GetUsersModel)
@scoped((UserRole.ADMIN.value,), require_all=False)
@protected()
async def get_users(request: Request, query: GetUsersModel):  # pylint: disable=unused-argument
    users = await loaders.users_query(limit=query.limit, offset=query.offset).all()
    return json([user.to_dict() for user in users])


@blueprint.get('/<user_id:uuid>')
@openapi.operation('getUser')
@openapi.summary(f'[{UserRole.ADMIN.value},{UserRole.USER.value}] Get user by ID')
@openapi.secured('token')
@openapi.parameter('user_id', UserSchema.id, 'path', required=True, allowEmptyValue=False)
@openapi.response(200, {'application/json': UserSchema}, description='OK')
@openapi.response(400, {'application/json': ResponseSchema}, description='Bad Request')
@openapi.response(401, {'application/json': AuthErrorSchema}, description='Unauthorized')
@openapi.response(403, {'application/json': AuthErrorSchema}, description='Forbidden')
@openapi.response(404, {'application/json': ResponseSchema}, description='Not Found')
@openapi.response(500, {'application/json': ResponseSchema}, description='Internal Server Error')
@protected()
@scoped([UserRole.ADMIN.value, UserRole.USER.value], False)
@inject_user()
async def get_user(request: Request, user: User, user_id: str):  # pylint: disable=unused-argument
    is_admin = user.is_admin
    if not is_admin:
        user_id = user.id

    user = await loaders.users_query(user_id=user_id).first_or_404()
    return json(user.to_dict())


@blueprint.post('/register')
@openapi.operation('addUser')
@openapi.summary('Add new user')
@openapi.body({"application/json": AddUserSchema}, required=True)
@openapi.response(200, {'application/json': UserSchema}, description='OK')
@openapi.response(400, {'application/json': ResponseSchema}, description='Bad Request')
@openapi.response(500, {'application/json': ResponseSchema}, description='Internal Server Error')
@validate(json=AddUserModel)
async def add_user(request: Request, body: AddUserModel):
    user = await loaders.users_query(email=body.email.lower()).all()
    if user:
        raise InvalidUsage('User already exists')

    password = await request.app.ctx.password_hasher.async_hash(body.password)
    user = await User.create(email=body.email.lower(), password=password)

    if body.role == UserRole.ADMIN.value:
        await user.update(role=UserRole.ADMIN.value).apply()

    return json(user.to_dict())


@blueprint.patch('/<user_id:uuid>')
@openapi.operation('updateUser')
@openapi.summary(f'[{UserRole.ADMIN.value},{UserRole.USER.value}] Update user by ID')
@openapi.secured('token')
@openapi.parameter('user_id', UserSchema.id, 'path', required=True, allowEmptyValue=False)
@openapi.body({"application/json": UpdateUserSchema}, required=True)
@openapi.response(200, {'application/json': UserSchema}, description='OK')
@openapi.response(400, {'application/json': ResponseSchema}, description='Bad Request')
@openapi.response(401, {'application/json': AuthErrorSchema}, description='Unauthorized')
@openapi.response(403, {'application/json': AuthErrorSchema}, description='Forbidden')
@openapi.response(404, {'application/json': ResponseSchema}, description='Not Found')
@openapi.response(500, {'application/json': ResponseSchema}, description='Internal Server Error')
@validate(json=UpdateUserModel)
@protected()
@scoped([UserRole.USER.value, UserRole.ADMIN.value], False)
@inject_user()
async def update_user(request: Request, body: UpdateUserModel, user_id: str, user: User):
    is_admin = user.is_admin
    if not is_admin:
        user_id = str(user.id)
    user = await loaders.users_query(user_id=user_id).first_or_404()

    user_update = None
    if body.email:
        user_update = (user_update or user).update(email=body.new_email)
    if body.new_password:
        password_hash = request.app.ctx.password_hasher.async_hash(body.new_password)
        user_update = (user_update or user).update(password=password_hash)
    if user_update:
        user_update = user_update.update(update_datetime=datetime.utcnow())
        await user_update.apply()

    return json(user.to_dict())


@blueprint.delete('/<user_id:uuid>')
@openapi.operation('deleteUser')
@openapi.summary(f'[{UserRole.ADMIN.value}] Delete user by ID')
@openapi.secured('token')
@openapi.parameter('user_id', UserSchema.id, 'path', required=True, allowEmptyValue=False)
@openapi.response(200, {'application/json': ResponseSchema}, description='OK')
@openapi.response(400, {'application/json': ResponseSchema}, description='Bad Request')
@openapi.response(401, {'application/json': AuthErrorSchema}, description='Unauthorized')
@openapi.response(403, {'application/json': AuthErrorSchema}, description='Forbidden')
@openapi.response(404, {'application/json': ResponseSchema}, description='Not Found')
@openapi.response(500, {'application/json': ResponseSchema}, description='Internal Server Error')
@protected()
@scoped((UserRole.ADMIN.value,), require_all=False)
async def delete_user(request: Request, user_id: str):  # pylint: disable=unused-argument
    user = await loaders.users_query(user_id=user_id).first_or_404()
    await user.update(update_datetime=datetime.utcnow(), is_deleted=True).apply()

    return json({'status': 200, 'message': 'OK', 'description': None})
