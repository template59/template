import pytest
from sanic import Sanic

from database import User, UserRole


@pytest.mark.asyncio
async def test_add_user_admin(app: Sanic):
    _, response = await app.asgi_client.post('/api/users/register', json={
                                                                        'email': 'test',
                                                                        'password': await app.ctx.password_hasher.async_hash('password'),
                                                                        'role': UserRole.USER.value })
    assert response.status == 200


@pytest.mark.asyncio
async def test_add_user_invalid_login(app: Sanic, invalid_logins):
    _, response = await app.asgi_client.post('/api/users/register', json={
                                                                        'email': invalid_logins,
                                                                        'password': await app.ctx.password_hasher.async_hash('password'),
                                                                        'role': UserRole.USER.value
                                                                        })
    assert response.status == 400
