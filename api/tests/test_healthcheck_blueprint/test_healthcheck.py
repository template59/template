import pytest
from sanic import Sanic

from database import User


@pytest.mark.asyncio
async def test_healthcheck_no_data(app: Sanic):
    _, response = await app.asgi_client.get('/api/healthcheck')
    assert response.status == 200
    assert response.json == {'status': 200, 'message': 'OK', 'description': None}
